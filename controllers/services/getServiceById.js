'use strict';

const getDB = require('../../BBDD/getConnection');
const { generateError } = require('../../helpers');

const getServicesById = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { id } = req.params;

    const [services] = await connection.query(
      `
      SELECT *
      FROM services
      WHERE id = ?
    `,
      [id]
    );

    if (services.length < 1) {
      generateError('No existen este servicio concreto', 500);
    }

    const infoService = {
      title: services[0].title,
      description: services[0].description,
      file_name: services[0].file_name,
      solved: services[0].solved,
      created_at: services[0].created_at,
      modified_at: services[0].modified_at,
    };

    const [comments] = await connection.query(
      `
      SELECT id, comment
      FROM comments
      WHERE services_id = ?
      `,
      [id]
    );
    /* 
    // - Declaramos un array vacio, donde guardaremos posteriormente los comentarios.
    let dataComments = [];

    // - Recorremos el array 'services' para extraer los comentarios.
    for (let i = 0; i < comments.length; i++) {
      const data = Object.values(comments[i]);
      dataComments.push(data[0]);
    }

    // - Filtramos los comentarios, para que no se nos acumulen repetidos los mismos comentarios cada
    // vez que hacemos la misma petición.
    let dataFiltered = dataComments.filter((item, i) => {
      return dataComments.indexOf(item) === i;
    });

    // - Añadimos el array de comentarios al objeto 'infoService'.
    infoService.comments = dataFiltered; */

    infoService.comments = comments;

    res.status(200).send({
      status: 'ok',
      message: 'Información de un servicio concreto',
      data: infoService,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getServicesById;
