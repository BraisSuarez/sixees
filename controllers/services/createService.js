'use strict';

const getDB = require('../../BBDD/getConnection');
const { generateError, saveFile } = require('../../helpers');
const path = require('path');

const createService = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();
    const { title, description } = req.body;

    if (!title || !description || !req.files?.file) {
      generateError('Faltan campos', 400);
    }

    // - Guardamos la imagen en la carpeta "uploads" y obtenemos el nombre de la misma.
    const fileName = await saveFile(req.files.file);

    // - Guardamos el service en la BBDD.

  const [existingUser] = await connection.query(
      `
      INSERT INTO services (title, description, file_name, users_id )
      VALUES (?, ?, ?, ?)
  `,
      [title, description, fileName, req.user.id]
    );

  
    res.send({
      status: 'ok',
      message: 'Servicio creado',
    });
  } catch (err) {
    next(err);
  }
};

module.exports = createService;
