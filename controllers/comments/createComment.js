'use strict';

const getDB = require('../../BBDD/getConnection');
const { generateError, saveFile } = require('../../helpers');
const path = require('path');

const createComment = async (req, res, next) => {
  // - Abrimos la conexión.
  let connection;

  try {
    // - Conectamos con la BBDD.
    connection = await getDB();

    // - Extreamos 'id' y 'comment' de 'path-params' y el 'body' respectivamente.
    const { id } = req.params;
    const { comment } = req.body;

    if (!comment) {
      // - El comentario debe ser obligatorio de lo contrario generamos un error.
      generateError('Faltan campo comentario', 400);
    }

    if (req.files?.file) {
      // - Guardamos el archivo  en la carpeta "uploads" en caso de existir y obtenemos el nombre de la misma.
      const fileName = await saveFile(req.files.file);

      // - Generamos la query a insertar en el BBDD en el caso de que exista un archivo a parte del comentario.
      const [comments] = await connection.query(
        `
        INSERT INTO comments (comment, users_id, services_id, file_name)
        VALUES (?, ?, ?, ?)
    `,
        [comment, req.user.id, id, fileName]
      );
    } else {
      // - En el caso de que no exista un archivo generamos la query del 'comment' en el BBD.
      const [comments] = await connection.query(
        `
            INSERT INTO comments (comment, users_id, services_id)
            VALUES (?, ?, ?)
        `,
        [comment, req.user.id, id]
      );
    }

    // - Guardamos el 'comment' en la BBDD con el objeto res.
    res.send({
      status: 'ok',
      message: 'Comentario creado',
    });
  } catch (err) {
    next(err);
  }
};

module.exports = createComment;
