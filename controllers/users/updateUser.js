'use strict';

const getDB = require('../../BBDD/getConnection');
const { generateError } = require('../../helpers');

/*
 **** El usuario podra modificar: 
 ****  Email, nombre, apellido, biografia y avatar.
 ****
 */
const updateUser = async (req, res, next) => {
  let connection;

  try {
    // Realizamos la conexion a la base de datos.
    connection = await getDB();

    // Tomamos el "id" que viene por parametro.
    // en este caso es el usuario. 
    const { id } = req.params;

    // Destructuring de los datos mediante el body
    // Estos datos seran los nuevos. 

    const { name, last_name, email, bio } = req.body;

    // Comprobamos que el usuario pasado por parametros sea el mismo que 
    // el del token. 
    if(req.user.id !== Number(id)){
        generateError('No tienes permisos para editar el usuario.', 403);
    }

    // Buscamos el usuario en la base de datos. 

    const [user] = await connection.query(
        `
        SELECT *
        FROM users
        WHERE id = ?
        `,
        [id]
    );

    // Modifcacion del email

    
    if(email !== user[0].email && email){

        // Si el usuario desea modificar su email, debemos comprobar que no existe
        // en la base de datos. 
        const [newEmail] = await connection.query(
             `
            SELECT id
            FROM users
            WHERE email=?               
            `,
            [email]
        );

        // Si existe el email proporcionado por el usuario, no podemos modifcaro.
        if(newEmail.length > 0){
            generateError('Ya existe un usuario con ese email.', 403);
         }

         // Modificamos el email.

         await connection.query(
          `
          UPDATE users
          SET email = ?
        `
        ,[email]
        );
         
       
    }

        
    // Realizamos el update de los datos del usuario. 

    await connection.query(
      `
      UPDATE users
      SET name = ?, last_name = ?, modified_at=?, bio = ?
    `
    ,[name, last_name, new Date(), bio, id]
    );

    

    res.status(200).send({
      status: 'ok',
      message: 'Usuario modificado',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = updateUser;
