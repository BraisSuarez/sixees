'use strict';

const getDB = require('../../BBDD/getConnection');

const infoUser = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { id } = req.params;

    const [user] = await connection.query(
      `
      SELECT email, created_at, name, last_name, bio, img
      FROM users
      WHERE id=?
    `,
      [id]
    );

    const [services] = await connection.query(
      `
      SELECT title, description, file_name, solved, created_at
      FROM services
      WHERE users_id = ?
    `,
      [id]
    );

    const info = {
      name: user[0].name,
      last_name: user[0].last_name,
      bio: user[0].bio,
      img: user[0].img,
      created_at: user[0].created_at,
      email: user[0].email,
      services: services,
    };

    res.status(200).send({
      status: 'ok',
      message: 'Informaciones usuario',
      data: info,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = infoUser;
